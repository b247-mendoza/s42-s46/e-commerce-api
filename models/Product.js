const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		require : [true, "Product Name is required!"]
	},
	description : {
		type : String,
		require : [true, "Description  is required!"]
	},
	price : {
		type: Number,
		require: [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	stocks : {
		type: Number,
		require : [true, "Stocks is required to be active"]
	},
	createdOn : {
		type : Date,
		default : new Date()
	}
})

module.exports = mongoose.model("Product", productSchema);
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes")

const app = express();

const port = 4000;

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://mainemendoza:mavis@zuitt-bootcamp.waf5lts.mongodb.net/s42-s46?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// CORS stands for Cross-Origin Resources Sharing.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => console.log(`API is now online on port ${port}`));
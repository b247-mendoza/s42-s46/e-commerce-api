const Order = require("../models/Order");

module.exports.orderCheckOut = async (data) => {
		
	if (data.isAdmin == true) {
		let m = "User is admin cannot proceed"
		return m;
	} else {
		let newOrder = new Order({
			userId : data.userId,
			products:{
				productId: data.productId,
				quantity : data.quantity,
			},
			totalAmount : data.totalAmount
		});

		return newOrder.save().then((order, err) =>{
			let message = "";
			if (err) {
				message = "Failed to checkout order!"
				return message;
			} else {
				message = "Order checked out successfully!"
				return message;
			};
		});


	}
};
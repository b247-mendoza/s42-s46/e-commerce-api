const Product = require("../models/Product");


// Adding a product
module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			stocks : data.product.stocks
		});

		return newProduct.save().then((product, err) => {
			let message = "";
			if (err) {
				message = "Adding product failed!"
				return message;
			} else {
				message = "Product Added!"
				return message;
			};
		});
	}
	let warning = Promise.resolve({
		warning : "User must be an Admin to add products!"
	});

	return warning.then((value) => {
		return value;
	});
};


// Retrieving All Products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Retrieving All Active Products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	});
};


// Retrieving a Single Product Details
module.exports.getOneProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Updating Product Information
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		stocks : reqBody.stoks
	}

	return Product.findByIdAndUpdate(reqParams.id, updatedProduct).then((product, err) => {
		let message = "";
		if (err) {
			message = "Error Occured. Product Update Failed."
			return message;
		} else {
			message = `Successfully updated product ID - "${reqParams.id}"`
			return message;
		};
	});
};


// Archiving a Product
module.exports.archiveProduct = (reqParams) => {
	let archivedProduct = {
		isActive : false
	}
	return Product.findByIdAndUpdate(reqParams.id, archivedProduct).then((product, err) => {
		let message = "";
		if (err) {
			message = "Archiving Product Failed!"
			return message;
		} else {
			message = "Product successfully archived."
			return message;
		};
	});
};


// Reactivating archived products
module.exports.activateProduct = (reqParams) => {
	let activatedProduct = {
		isActive : true
	}
	return Product.findByIdAndUpdate(reqParams.id, activatedProduct).then((product, err) => {
		let message = "";
		if (err) {
			message = "Activating Product Failed!"
			return message;
		} else {
			message = "Product activated!"
			return message;
		};
	});
};
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

// Registering a User
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		userName : reqBody.userName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user,err) => {
		let message = "";
		if (err) {
			message = "Error found data cannot save!"
			return message;
		} else {
			message = "New user has been registered!"
			return message;
		};
	});
};

// Login a User
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		let message = "";
		if(result == null) {
			message = "email does not exists!"
			return message;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				message = "User successfully logged in"
				return { access : auth.createAccessToken(result), message};
			} else {
				message = "Incorrect Password"
				return message;
			};
		};
	});
};

// Setting User as Admin
module.exports.setUserAsAdmin = (reqParams, reqBody) => {
	let updatedRole = {
		isAdmin : reqBody.isAdmin,
	}
	return User.findByIdAndUpdate(reqParams.id, updatedRole).then((result,err) => {
		let message = "";
			if (err) {
				message = "Error Occured. isAdmin role was not updated."
				return message;
			} else {
				message = `isAdmin role was successfully updated for product ID - "${reqParams.id}"`
				return message;
			};
	});
};

// Retrieving All Users
module.exports.getUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
} 

// Retrieving User Details
module.exports.getProfile = (reqAuth) => {
	return User.findById(reqAuth.id).then(result =>{
		let message = "";
		if (result == null) {
			message = "No user exists!"
		} else{
			result.password = "";
			return result;
		}

	});
};
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

// Creating a Product route
router.post('/add', auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Retrieving all products route
router.get('/all-products', (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// Retrieving all active products route
router.get('/active-products', (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// retrieving single product details route
router.get('/product-details/:productId', (req, res) => {
	productController.getOneProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Updating a product route
router.put('/update/:id', auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Archiving a product route
router.put('/archive/:id', auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Activating a product route
router.put('/activate/:id', auth.verify, (req, res) => {
	productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
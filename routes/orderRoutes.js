const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.post('/checkout', auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).userId,
		productId: req.body.productId,
		quantity: req.body.quantity,
		totalAmount: req.body.totalAmount
	}

	 orderController.orderCheckOut(req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
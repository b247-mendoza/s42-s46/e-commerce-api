const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");

// Registration route
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Login Route
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Retrieving All Users
router.get("/all-users", (req,res) => {
	userController.getUsers().then(resultFromController => res.send(resultFromController));
});


// Setting User to Admin
router.put("/settings/:id", auth.verify, (req, res) => {
	userController.setUserAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// User Details route
router.get("/user-details", auth.verify, (req, res) => {
	let userDetails = auth.decode(req.headers.authorization);
	userController.getProfile(userDetails).then(resultFromController => res.send(resultFromController));
});

module.exports = router;